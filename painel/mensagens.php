<?php
session_start();
include_once("../connect.php");
if (empty($_SESSION['id'])) {
	$_SESSION['mensagem']="Você não tem permissão aqui invocador";
	header("location: ../");
	exit;
}else{
}
if (!empty($_SESSION['mensagem'])) {
	$mensagem=$_SESSION['mensagem'];
?>
<script type="text/javascript">
alert("<?=$mensagem?>");
</script>
<?php
unset($_SESSION['mensagem']);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="shortcut icon" type="image/x-icon" href="../7880icone.ico">
		<title>Não Feedo - Novo Participante</title>
		
		<meta charset="utf-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../estilo.css">
		<script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/bootstrap.js"></script>
	</head>
	<body>
		<div class="menunaofeedo">
			<a href="index.php"><img src="../logolado.png" width="10%"></a>
			
		</div>
		<br><br>
		<div class="container">
			<div class="row jumbotron">
				<div class="col-xs-10 col-sm-10 col-md-3 col-lg-3">
							<a href="mensagens.php">
								<?php
								$comando="SELECT * FROM mensagens";
								$enviar=mysqli_query($conn, $comando);
								$recebe=mysqli_fetch_all($enviar);
								$contador=0;
								if ($recebe) {
									foreach ($recebe as $resultado) {
										$contador=$contador+1;
									}
								}
								?>
							<button class="btn btn-default" style="width: 100%">Mensagens <?php
						if ($contador!=0) {
						?>
						<span class="badge"><?=$contador?></span>
						<?php
						}
						?>
					</button>
						</a><br><br>
							<a href="novoep.php">
								<button class="btn btn-info" style="width: 100%">Adicionar Novo Episódio</button>
							</a>
							<br><br>
							<a href="novopar.php">
								<button class="btn btn-info" style="width: 100%">Adicionar Novo Participante</button>
							</a>
							<br><br>
							<a href="apagarep.php">
								<button class="btn btn-info" style="width: 100%">Apagar Episodio</button>
							</a>

							<br><br>
							<a href="../">
							<button class="btn btn-danger" style="width: 100%">Voltar ao site</button>
						</a>
							
						</div>
						<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 thumbnail">
							<center><font size="3">Mensagens</font>
							<br><br>
						<table class="table table-inverse">
								<thead>
									<tr>
										<th>Nome do Episódio</th>
										<th><center>Apagar Mensagem</center></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$busca="SELECT * FROM mensagens ORDER BY id";
									$enviar=mysqli_query($conn, $busca);
									$recebe=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
									if ($recebe) {
									foreach ($recebe as $mensagem) {
										$id=$mensagem['id'];
										$conteudo=$mensagem['conteudo'];
									?>
									<tr>
										
										<td><?=utf8_encode($conteudo)?></td>
										<td>
											<center>
										<form action="apagarm.php" method="post" accept-charset="utf-8" name="apagarep">
											<input type="hidden" name="idmensagem" value="<?=$id?>">
											<button value="excluir" class="btn btn-danger">
											X</button>
										</form>
									</center>
										</td>
									</tr>
									<?php
								}
							}else{
									?>
									<td>Nenhuma mensagem</td>;
									<?php
								}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-xs-1 col-sm-1 col-md-0 col-lg-0"></div>
			</div>
		</div>
	</body>
</html>