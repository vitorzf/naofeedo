<?php
session_start();
include_once("../connect.php");
if (empty($_SESSION['id'])) {
	$_SESSION['mensagem']="Você não tem permissão aqui invocador";
	header("location: ../");
	exit;
}else{
}
if (!empty($_SESSION['mensagem'])) {
	$mensagem=$_SESSION['mensagem'];
?>
<script type="text/javascript">
alert("<?=$mensagem?>");
</script>
<?php
unset($_SESSION['mensagem']);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="shortcut icon" type="image/x-icon" href="../7880icone.ico">
		<title>Não Feedo - Novo Episódio</title>
		
		<meta charset="utf-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../estilo.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="menunaofeedo">
			<a href="index.php"><img src="../logolado.png" width="10%"></a>
			
		</div>
		<br><br>
		<div class="container">
			<div class="row jumbotron">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row">
						<div class="col-xs-10 col-sm-10 col-md-3 col-lg-3">
							<a href="mensagens.php">
								<?php
								$comando="SELECT * FROM mensagens";
								$enviar=mysqli_query($conn, $comando);
								$recebe=mysqli_fetch_all($enviar);
								$contador=0;
								if ($recebe) {
									foreach ($recebe as $resultado) {
										$contador=$contador+1;
									}
								}
								$comando="SELECT * FROM podcast";
								$enviar=mysqli_query($conn, $comando);
								$recebe=mysqli_fetch_all($enviar);
								$proximo=0;
								if ($recebe) {
									foreach ($recebe as $resultado) {
										$proximo=$proximo+1;
									}
								}
								?>
							<button class="btn btn-default" style="width: 100%">Mensagens <?php
						if ($contador!=0) {
						?>
						<span class="badge"><?=$contador?></span>
						<?php
						}
						?>
					</button>
						</a><br><br>
							<a href="novoep.php">
								<button class="btn btn-info" style="width: 100%">Adicionar Novo Episódio</button>
							</a>
							<br><br>
							<a href="novopar.php">
								<button class="btn btn-info" style="width: 100%">Adicionar Novo Participante</button>
							</a>
							<br><br>
							<a href="apagarep.php">
								<button class="btn btn-info" style="width: 100%">Apagar Episodio</button>
							</a>

							<br><br>
							<a href="../">
							<button class="btn btn-danger" style="width: 100%">Voltar ao site</button>
						</a>
							
						</div>
						<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 thumbnail">
							<center><font size="3">Adicionar novo Episódio</font>
							<br><br>
				<form enctype="multipart/form-data" action="upload.php" method="post" accept-charset="utf-8">
					<input type="hidden" name="proximo" value="<?=$proximo+1?>">
						Nome do Podcast <br>
						<input type="text" name="nomepodcast"><br><br>
						Foto da Capa (600x600)<br>
						<input type="file" name="capa"><br><br>
						Descrição<br>
						<textarea name="descricao" style="min-width:50%;max-width: 100%;height: 100px;" placeholder="Para uma descrição mais bem feita use tags como <strong> para deixar alguma escrita em negrito, ou <br> para pular linhas, nao esqueça que o <strong> precisa ser fechado com </strong>"></textarea><br><br>
						Arquivo MP3<br>
						<input type="file" name="audio"><br><br>
						Menções<br>
						<textarea name="mencoes" style="min-width:50%;max-width: 100%;height: 100px;" placeholder="Leia antes de digitar, para colocar menções você deve colocar o link do seguinte jeito, '<a href='LINK DA MENCAO'>Coisa digitada para a pessoa clicar</a>"></textarea><br><br>
						<font size="3">Participantes do podcast</font><br><br>
					<div>
					<?php
					$comando="SELECT * FROM participantes ORDER BY id_participante";
					$enviar=mysqli_query($conn, $comando);
					$recebe=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
					for ($i=1; $i <=5; $i++) { 
					?>
					Participante <?=$i?>
					<select name="part<?=$i?>">
					<option value="0">Selecionar</option>
					<?php
					foreach ($recebe as $participante) {
						$idpar=$participante['id_participante'];
						$nomepar=$participante['nome'];
					?>
					<option value="<?=$idpar?>"><?=$nomepar?></option>
					<?php
					}
					?>
					</select><br>
					<?php
					}
					?><br>
					<button type="submit" name="concluir" class="btn btn-primary">Enviar</button>

					<br><br>
  				
				</div>
				</div>
					</center>
				</form>
						</div>
					</div>
				</div>
				<div class="col-xs-1 col-sm-1 col-md-0 col-lg-0"></div>
			</div>
		</div>
	</body>
</html>