<?php
session_start();
require_once("connect.php");
if (!empty($_SESSION['mensagem'])) {
	$mensagem=$_SESSION['mensagem'];
	?>
	<?php
	unset($_SESSION['mensagem']);
}
	
	if (!empty($_GET['cod'])) {
		$episodio=base64_decode(base64_decode($_GET['cod']));
		$busca="SELECT * FROM podcast WHERE id = '$episodio'";
		$enviar=mysqli_query($conn, $busca);
		$podcast=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
		foreach ($podcast as $podcast) {
		$nomepod=$podcast['nome'];
		$foto=$podcast['Imagem'];
		$descricao=$podcast['descricao'];
		$mencoes=$podcast['mencoes'];
		$p[1]=$podcast['p1'];
		$p[2]=$podcast['p2'];
		if (!empty($podcast['p3'])) {
			$p[3]=$podcast['p3'];
		}
		if (!empty($podcast['p4'])) {
			$p[4]=$podcast['p4'];
		}
		if (!empty($podcast['p5'])) {
			$p[5]=$podcast['p5'];
		}
		$arquivo=$podcast['arquivo'];
?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="shortcut icon" type="image/x-icon" href="7880icone.ico">
		<title><?=utf8_decode(utf8_encode($nomepod))?></title>
		
		<meta charset="utf-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="estilo.css">
		
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
	</head>
	<body>
		
	
		<?php
		include_once("head.php");
		//verifica se a variavel pagina existe
		?>
			<center>
		<div class="podcastescolhido">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<center>
					<img src="imagem/<?=$foto?>" width="90%" style="margin-top: 10px;">
					</center>
					<br>
<div class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block">
				
						<font face="calibri" size="5" color="white">Participantes</font><br>
					
					<?php
					foreach ($p as $participante) {
						$procura="SELECT * FROM participantes WHERE id_participante = '$participante'";
						$enviar=mysqli_query($conn, $procura);
						$resul=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
						foreach ($resul as $perfil) {
							$foto=$perfil['foto'];
							$nome=$perfil['nome'];
					?>
					<center>
					<img src="participantes/<?=$foto?>" width="80px" class="img-circle"><br> 
					<font face="calibri" size="5" color="white"><?=$nome?></font>
			    	</center>
					
					<br>
					<?php
					}
					}
					?>
				</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
			<font face="calibri" size="5" color="white"><?=utf8_decode(utf8_encode($nomepod))?></font><br>
					<hr>
					<br>
					<font face="calibri" size="4" color="white"><?=utf8_decode(utf8_encode($descricao))?></font><br>
					<script type="text/javascript">
					function daplay(){
					var arquivo = document.getElementById("naofeedo");
					var play = document.getElementById("play");
					if (arquivo.paused) {
						arquivo.play();
						document.getElementById("play").innerHTML="<span class='glyphicon glyphicon-pause'></span>";
					}else{
						arquivo.pause();
						document.getElementById("play").innerHTML="<span class='glyphicon glyphicon-play'></span>";
					}
					}
					</script>
					<br><br>
					<div>
						<audio id="naofeedo">
							<source src="audios/<?=$arquivo?>" type="audio/mpeg">
							Seu navegador não suporta áudio em HTML5, atualize-o.
						</audio>
					</div>
					<center>
					
						<font face="calibri" size="4" color="white">
						<button onclick="daplay()" id="play" class="botoes">
						<span class="glyphicon glyphicon-play"></span></button>
						<button id="mais" class="botoes" onclick="document.getElementById('naofeedo').currentTime=0">
						<span class="glyphicon glyphicon-stop"></span>
						</button>
						<button id="mais" class="botoes" onclick="document.getElementById('naofeedo').volume+=0.1">
						<span class="glyphicon glyphicon-volume-up"></span>
						</button>
						<button id="menos" class="botoes" onclick="document.getElementById('naofeedo').volume-=0.1">
						<span class="glyphicon glyphicon-volume-down"></span>
						</button>
						<button id="ma10" onmousedown="document.getElementById('naofeedo').currentTime-=10" class="botoes"><span class="glyphicon glyphicon-backward"></span></button>
						<button id="me10" onmousedown="document.getElementById('naofeedo').currentTime+=10" class="botoes"><span class="glyphicon glyphicon-forward"></span></button><br><br>
					</font>
					<br><br>
					<a href="audios/<?=$arquivo?>" download="<?=utf8_decode(utf8_encode($nomepod))?>"><button class="btn" style="background-color: #491aaf;"><span class="glyphicon glyphicon-download"></span> Download</button></a>
					
				</center>
				</div>
				
				<div style="margin-top:50px" class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<font face="calibri" size="5" color="white">Menções no Podcast</font><br><br>
				<?=utf8_decode(utf8_encode($mencoes))?>
				<br><br><br><br><br><br><button style="margin-bottom: 10px;" type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">Enviar Feedback</button>
				</div>
				<br><br>
				<div class="visible-xs-block visible-xs-inline visible-xs-inline-block visible-sm-block visible-sm-inline visible--inline-block">
					<font face="calibri" size="5" color="white">Participantes</font><br><br>
				<?php

					foreach ($p as $participante) {
						$procura="SELECT * FROM participantes WHERE id_participante = '$participante'";
						$enviar=mysqli_query($conn, $procura);
						$resul=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
						foreach ($resul as $perfil) {
							$foto=$perfil['foto'];
							$nome=$perfil['nome'];
					?>
					
					<center>
						
					<img src="participantes/<?=$foto?>" width="80px" class="img-circle"> 
					<font face="calibri" size="5" color="white"><?=utf8_decode(utf8_encode($nome))?></font>
					</center>
					
					<br>
					<?php
					}
					}
					?>
				<br>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#ajax_form').submit(function(){
			var dados = jQuery( this ).serialize();

			jQuery.ajax({
				type: "POST",
				url: "enviarmensagem.php",
				data: dados,
				success: function( data )
				{
					alert( "Obrigado pela sugestão" );
				}
			});
			
			return false;
		});
	});
	</script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enviar Sugestão</h4>
      </div>
      <div class="modal-body">
        <form action="" method="post" accept-charset="utf-8" id="ajax_form">
        	<textarea name="texto" style="min-width:50%;max-width: 100%;height: 200px;"></textarea><br>
        	<button type="submit" class="btn btn-default" id="enviaDados">Enviar</button>
        </form>
        
      </div>
      <div class="modal-footer">
          <font size="1">Caso queira enviar alguma imagem ou video envie para igovitorbr@gmail.com</font> 
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
        <br>
       
      </div>
    </div>

  </div>
</div>
</div>
</div>
</center>
				<?php
				}
				}
				?>
			</body>
		</html>