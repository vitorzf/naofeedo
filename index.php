<?php
session_start();
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" type="image/x-icon" href="7880icone.ico">
	<title>Não Feedo - Inicio</title>
	
	<meta charset="utf-8">
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
	<?php

if (!empty($_SESSION['mensagem'])) {
	$mensagem=$_SESSION['mensagem'];
	?>
	<script type="text/javascript">
		alert("<?=$mensagem?>");
	</script>
	<?php
	session_destroy();
}
include_once("head.php");
	?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<?php
	//verifica se a variavel pagina existe
	$pagina=(isset($_GET['pagina']))? $_GET['pagina'] : 1 ;
 	$busca="SELECT * FROM podcast ORDER BY id DESC";
	$resultado=mysqli_query($conn, $busca);
	//contar resultado
	$total_podcast = mysqli_num_rows($resultado);
	//Quantidade maxima por página
	$maximo=6;
	//calcular o numero de paginas necessarias
	$num_paginas=ceil($total_podcast/$maximo);
	//Calcular inicio
	$inicio=($maximo*$pagina)-$maximo;
	//selecionar
	$resultado_final="SELECT * FROM podcast limit $inicio, $maximo";
	$resultado2=mysqli_query($conn, $resultado_final);
	$total=mysqli_num_rows($resultado2);
	$podcast=mysqli_fetch_all($resultado, MYSQLI_ASSOC);
	foreach ($podcast as $podcast) {
	$id=base64_encode(base64_encode($podcast['id']));
	$nome=$podcast['nome'];
	$foto=$podcast['Imagem'];

	?>
			<div class="podcast">
	<form action="ouvir.php" method="get" accept-charset="utf-8">
	<input type="hidden" name="cod" value="<?=$id?>">
	<button type="submit" style="background-color: transparent;border:none;">
		<img src="imagem/<?=$foto?>" style="margin-top: 5px; width: 240px;" ><br>
	<font face="calibri" size="5" color="white"><?php
	$novonome=utf8_decode(utf8_encode($nome));
	echo mb_strimwidth("$novonome", 0, 50, "...");
	?><br>
	
	</font>
	</button>
	</form>
			</div>
			


<?php
}
?>
</div>
	</div>

</body>
</html>